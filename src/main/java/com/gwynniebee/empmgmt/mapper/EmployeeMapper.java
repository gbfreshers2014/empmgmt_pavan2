/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.empmgmt.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.gwynniebee.empmgmt.pojos.Employee;

/**
 * @author pavan
 */
public class EmployeeMapper implements ResultSetMapper<Employee> {

    /*
     * (non-Javadoc)
     * @see org.skife.jdbi.v2.tweak.ResultSetMapper#map(int, java.sql.ResultSet,
     * org.skife.jdbi.v2.StatementContext)
     */
    @Override
    public Employee map(int index, ResultSet r, StatementContext ctx) throws SQLException {
        Employee e = new Employee();
        e.setEmpId(r.getInt("empId"));
        e.setEmailId(r.getString("emailId"));
        e.setfName(r.getString("fName"));
        e.setPhone(r.getInt("phone"));
        e.setStatus(r.getInt("status"));
        return e;
    }

}
