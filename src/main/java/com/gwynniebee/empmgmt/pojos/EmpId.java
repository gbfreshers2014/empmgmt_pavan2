/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.empmgmt.pojos;

/**
 * @author pavan
 */
public class EmpId {

    private int empId;

    /**
     * @return employee id
     */
    public int getEmpId() {
        return empId;
    }

    /**
     * @param empId sets empId
     */
    public void setEmpId(int empId) {
        this.empId = empId;
    }

}
