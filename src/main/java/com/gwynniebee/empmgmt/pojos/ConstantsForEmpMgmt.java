/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.empmgmt.pojos;

/**
 * constants of the application.
 * @author pavan
 */
public final class ConstantsForEmpMgmt {

    /**
     * private constructor.
     */
    private ConstantsForEmpMgmt() {

    }

    public static final String DATABASE_SOURCE_NAME = "jdbc/empmgmt";
    public static final String DATABASE_DRIVER = "com.mysql.jdbc.Driver";
    public static final String DATABASE_EMPLOYEE_URL = "jdbc:mysql://localhost/emp_mgmt";
    public static final String DATABASE_USERNAME = "pavan";
    public static final String DATABASE_PASSWORD = "abcd1234";
    public static final String EMP_MGMT_CONFIGURATION_PROPERTY_PATH = "/empmgmt.properties";
}
