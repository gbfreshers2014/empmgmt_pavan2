/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.empmgmt.restlet;

import java.util.Properties;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.restlet.Application;
import org.restlet.data.MediaType;
import org.restlet.routing.Router;
import org.skife.jdbi.v2.DBI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.empmgmt.em.BaseEntityManager;
import com.gwynniebee.empmgmt.pojos.ConstantsForEmpMgmt;
import com.gwynniebee.empmgmt.restlet.resources.EmpResource;
import com.gwynniebee.iohelpers.IOGBUtils;
import com.gwynniebee.rest.service.restlet.GBRestletApplication;

/**
 * Employee Management service routing and controlling resources.
 * @author pavan
 */
public class EmpMgmtApplication extends GBRestletApplication {

    private static final Logger LOG = LoggerFactory.getLogger(EmpMgmtApplication.class);
    private static Validator validator;
    private Properties serviceProperties;
    private static DBI dbi;

    /**
     * (From Application.getCurrent)<br>
     * This variable is stored internally as a thread local variable and updated
     * each time a call enters an application.<br>
     * <br>
     * Warning: this method should only be used under duress. You should by
     * default prefer obtaining the current application using methods such as
     * {@link org.restlet.resource.Resource#getApplication()} <br>
     * @return The current application.
     */
    public static EmpMgmtApplication getCurrent() {
        return (EmpMgmtApplication) Application.getCurrent();
    }

    /**
     * @return DBI after getting data source registered.
     */
    public static DBI getDbi() {
        if (dbi == null) {
            LOG.debug("Before InventoryManagement.getDBI");
            setDbi(EmpMgmtApplication.getDBICurrentApplication());
            LOG.debug("got DBI from InventoryManagement");
        }
        return dbi;
    }

    /**
     * @param dbi setDBI
     */
    public static void setDbi(DBI dbi) {
        EmpMgmtApplication.dbi = dbi;
    }

    /**
     * @return Datasource
     */
    public static DBI getDBICurrentApplication() {

        // LOG.debug("Before Inventory Management.getDBICurrentApplication");
        return EmpMgmtApplication.getCurrent().getDataSourceRegistry().getMonitoredDBI(ConstantsForEmpMgmt.DATABASE_SOURCE_NAME);
    }

    /*
     * @return
     * @see org.restlet.Application#createInboundRoot()
     */
    @Override
    public synchronized Router createInboundRoot() {

        Router router = super.createInboundRoot();
        String resourceUrl = null;
        this.getMetadataService().setDefaultMediaType(MediaType.APPLICATION_JSON);

        resourceUrl = "/employee.json";
        router.attach(resourceUrl, EmpResource.class);
        LOG.debug("Attaching employee with " + resourceUrl);

        return router;
    }

    /*
     * (non-Javadoc)
     * @see org.restlet.Application#start()
     */
    @Override
    public synchronized void start() throws Exception {
        if (!this.isStarted()) {

            // Prepare Properties
            this.serviceProperties = IOGBUtils.getPropertiesFromResource(ConstantsForEmpMgmt.EMP_MGMT_CONFIGURATION_PROPERTY_PATH);
            // Prepare validation factory

            BaseEntityManager.setDbi(getDbi());
            ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
            EmpMgmtApplication.validator = factory.getValidator();
        }
        // below will make this.isStarted() true
        super.start();
    }

    /*
     * (non-Javadoc)
     * @see org.restlet.Application#stop()
     */
    @Override
    public synchronized void stop() throws Exception {
        if (!this.isStopped()) {
            LOG.info("Application.stop()");
        }
        // below will make this.isStopped() true
        super.stop();
    }

    /**
     * @return the serviceProps
     */
    public Properties getServiceProperties() {
        return this.serviceProperties;
    }

    /**
     * @return validator
     */
    public static Validator getValidator() {
        return validator;
    }
}
