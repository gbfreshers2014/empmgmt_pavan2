/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.empmgmt.restlet.resources;

import java.util.List;

import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.Put;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.barcode.upload.common.Constants;
import com.gwynniebee.empmgmt.em.EmployeeeEntityManager;
import com.gwynniebee.empmgmt.pojos.EmpId;
import com.gwynniebee.empmgmt.pojos.Employee;
import com.gwynniebee.empmgmt.response.EmployeesDetailResponse;
import com.gwynniebee.empmgmt.response.MyResponse;
import com.gwynniebee.empmgmt.restlet.EmpMgmtApplication;
import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;


/**
 * EmpMgmtResource class. To change this resource rename the class to the
 * required class
 * @author pavan
 */
public class EmpResource extends AbstractServerResource {

    private static final Logger LOG = LoggerFactory.getLogger(EmpResource.class);

    /*
     * (non-Javadoc)
     * @see org.restlet.resource.UniformResource#doInit()
     */
    @Override
    protected void doInit() {
        super.doInit();
    }

    /*
     * (non-Javadoc)
     * @see com.gwynniebee.rest.service.restlet.resources.GBRestletResourse#
     * getErrorEmailSubject()
     */
    @Override
    protected String getErrorEmailSubject() {
        return "Shipment Tracking Service - NetsuiteFileServerResource Error";
    }

    /*
     * (non-Javadoc)
     * @see com.gwynniebee.rest.service.restlet.resources.GBRestletResourse#
     * getErrorEmailFrom()
     */
    @Override
    protected String getErrorEmailFrom() {
        return ((EmpMgmtApplication) this.getApplication()).getServiceProperties().getProperty(Constants.ALERT_EMAIL_FROM_PROP);
    }

    /*
     * (non-Javadoc)
     * @see com.gwynniebee.rest.service.restlet.resources.GBRestletResourse#
     * getErrorEmailTo()
     */
    @Override
    protected String getErrorEmailTo() {
        return ((EmpMgmtApplication) this.getApplication()).getServiceProperties().getProperty(Constants.ALERT_EMAIL_TO_PROP);
    }

    /**
     * javadoc.
     * @return email {String}
     */
    protected String getInfoEmailFrom() {
        return ((EmpMgmtApplication) this.getApplication()).getServiceProperties().getProperty(Constants.INFO_EMAIL_FROM_PROP);
    }

    /**
     * javadoc.
     * @return email {String}
     */
    protected String getInfoEmailTo() {
        return ((EmpMgmtApplication) this.getApplication()).getServiceProperties().getProperty(Constants.INFO_EMAIL_TO_PROP);
    }

    /**
     * Implementation of a GET method.
     * @return "hello world" {String}.
     */
    @Get
    public EmployeesDetailResponse getEmployeeDetails() {
        LOG.debug("fetching employee details..");

        EmployeesDetailResponse edr = new EmployeesDetailResponse();
        List<Employee> ees = EmployeeeEntityManager.getInstance().getEmployeesDetail();
        edr.setEes(ees);
        getRespStatus().setCode(0);
        getRespStatus().setMessage("employee details fetched successfully");
        edr.setStatus(this.getRespStatus());
        return edr;
    }

    /**
     * @param emp employee object
     * @return response
     */
    @Post
    public MyResponse addEmployee(Employee emp) {
        LOG.debug("adding employee details :" + emp.getEmailId() + " " + emp.getfName());
        int ret = EmployeeeEntityManager.getInstance().addEmployee(emp);
        MyResponse resp = new MyResponse();
        getRespStatus().setCode(0);
        getRespStatus().setMessage("employee added");
        resp.setStatus(getRespStatus());
        return resp;
    }

    /**
     * @param empId employee id
     * @return response
     */
    @Put("json")
    public MyResponse deleteEmployee(EmpId empId) {
        // String eId=(String)this.getRequest().getAttributes().get("empId");
        LOG.debug("deleting employee details :" + empId.getEmpId());
        int ret = EmployeeeEntityManager.getInstance().deleteEmployee(empId.getEmpId());
        MyResponse resp = new MyResponse();
        getRespStatus().setCode(0);
        getRespStatus().setMessage("employee deleted");
        resp.setStatus(getRespStatus());
        return resp;
    }
}
