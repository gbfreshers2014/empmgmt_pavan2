/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.empmgmt.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.gwynniebee.empmgmt.test.serverresource.AllServerResourceTest;

/**
 * All test cases.
 * @author Jitender
 */
@RunWith(Suite.class)
@SuiteClasses({AllServerResourceTest.class,com.gwynniebee.empmgmt.test.dao.TestEmployeeDAO.class})
public class AllTests {

}
